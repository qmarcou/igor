---------------------------------------
**WE HAVE MOVED!**

**The official IGoR repository is now on GitHub:**

[https://github.com/qmarcou/IGoR][github_repo]
[github_repo]: https://github.com/qmarcou/IGoR

We hope this change will allow us a better communication with users (through github *Issues*) and better traceability of future IGoR releases!


---------------------------------------
